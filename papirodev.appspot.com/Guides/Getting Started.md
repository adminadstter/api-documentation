# Getting Started
---

### Crea un API key
1. [Ingresa al portal de Adstter](https://www.adstter.com/).
2. Ingrese a la sección de cuenta
![cuenta](account.png?raw=true "Cuenta")
3. Copia el API key.
![apiKey](apiKey.png?raw=true "Api Key")
4. Utiliza este apiKey para ejecutar los servicios de Adstter Digital Signage
